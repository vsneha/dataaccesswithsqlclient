USE [SuperheroesDb]
GO

/*******************************************************************************
   Populate Power Table
********************************************************************************/



INSERT INTO [dbo].[Power] ([Name], [Description] ) VALUES ((N'Strength') , (N'Super strength is one of the most basic traditional superhero powers. From the early days of comics, most heroes typically came with it.When a hero is stronger than any who oppose them, it makes victory a near certainty. ' ));
INSERT INTO [dbo].[Power] ([Name], [Description]) VALUES ((N'Speed') , (N'Being able to move quickly means that a hero is also one step ahead of the villain.' ));
INSERT INTO [dbo].[Power] ([Name], [Description]) VALUES ((N'Invisibility') , (N' Can create invisible force fields and turn the objects around to be invisible.' ));
INSERT INTO [dbo].[Power] ([Name], [Description]) VALUES ((N'IHealing') , (N'Hero Not only does expedite the time for wounds to repair but it also greatly increases the length of life of a hero.' ));


GO

