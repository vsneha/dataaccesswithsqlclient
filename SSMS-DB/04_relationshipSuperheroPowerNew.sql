USE [SuperheroesDb]
GO

/****** Object:  Table [dbo].[SuperheroPower]    Script Date: 24/08/2021 19.58.33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************
   Create Primary Key Unique Indexes
********************************************************************************/

CREATE TABLE [dbo].[SuperheroPower](
	[SuperheroId] [int] NOT NULL,
	[PowerId] [int] NOT NULL,
 CONSTRAINT [PK_SuperheroPower] PRIMARY KEY CLUSTERED 
(
	[SuperheroId] ASC,
	[PowerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/*******************************************************************************
   Create Foreign Keys
********************************************************************************/

ALTER TABLE [dbo].[SuperheroPower]  WITH CHECK ADD  CONSTRAINT [FK_SuperheroPower_PowerId] FOREIGN KEY([PowerId])
REFERENCES [dbo].[Power] ([Id])
GO

ALTER TABLE [dbo].[SuperheroPower] CHECK CONSTRAINT [FK_SuperheroPower_PowerId]
GO

ALTER TABLE [dbo].[SuperheroPower]  WITH CHECK ADD  CONSTRAINT [FK_SuperheroPower_SuperheroId] FOREIGN KEY([SuperheroId])
REFERENCES [dbo].[Superhero] ([Id])
GO

ALTER TABLE [dbo].[SuperheroPower] CHECK CONSTRAINT [FK_SuperheroPower_SuperheroId]
GO


