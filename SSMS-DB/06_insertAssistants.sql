USE [SuperheroesDb]
GO

/*******************************************************************************
   Populate Assistant Table
********************************************************************************/


INSERT INTO [dbo].[Assistant] ([Name] , [SuperheroId]) VALUES (N'Karen' , 2);
INSERT INTO [dbo].[Assistant] ([Name] , [SuperheroId]) VALUES (N'Argus', 3);
INSERT INTO [dbo].[Assistant] ([Name], [SuperheroId]) VALUES (N'Alfred', 4);


GO

