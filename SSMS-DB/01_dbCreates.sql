
use  master;
go

/*******************************************************************************
   Drop database if it exists
********************************************************************************/
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'SuperheroDb')
BEGIN
	ALTER DATABASE [SuperheroDb] SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE [SuperheroDb] SET ONLINE;
	DROP DATABASE [SuperheroDb];
END

GO

/*******************************************************************************
   Create database
********************************************************************************/
CREATE DATABASE [SuperheroDb];
GO
