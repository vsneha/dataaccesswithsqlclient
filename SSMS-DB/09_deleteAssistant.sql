
USE [SuperheroesDb]
GO


/*******************************************************************************
   DELETE Assistant Name
********************************************************************************/

create procedure DeletAssistantName(@Name nvarchar(150) )
as
begin;
declare @id int;
if not exists(select * from Assistant where SuperheroId=(select SuperheroId from Assistant where Name=@Name))
begin
delete from Assistant where Name=@Name;
end
if not exists(select * from Assistant where SuperheroId=@id)
begin
delete from Superhero where Id=@id;
end
end

GO
