﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLClientCRUD.Models;

namespace SQLClientCRUD.Repositories
{
    public interface ICustomerRepository
    {
        
        public List<Customer> GetAllCustomers();
        public Customer GetCustomerById(string id);
        public Customer GetCustomerByName(string firstName);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
       
    }
}
